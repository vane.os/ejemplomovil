package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    // crear la estructura

    private EditText txt1;
    private EditText txt2;
    private EditText txtRes;
    private Button btnSumar;
    private Button btnRestar;
    private Button btnMult;
    private Button btnDiv;
    private Button btnLimpiar;
    private Button btnCerrar;
    private Operaciones op = new Operaciones();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setEventos();


    }


    public void initComponents() {

        txt1 = (EditText) findViewById(R.id.txtNum1);
        txt2 = (EditText) findViewById(R.id.txtNum2);
        txtRes = (EditText) findViewById(R.id.txtRes);
        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMult = (Button) findViewById(R.id.btnMult);
        btnDiv = (Button) findViewById(R.id.btnDivi);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        setEventos();


    }

    public void setEventos() {

        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnDiv.setOnClickListener(this);
        this.btnMult.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnSuma:
                if (!txt1.getText().toString().isEmpty() && !txt2.getText().toString().isEmpty()) {
                    op.setNum1(Float.parseFloat(txt1.getText().toString().trim()));
                    op.setNum2(Float.parseFloat(txt2.getText().toString().trim()));
                    txtRes.setText(Float.toString(op.suma()));
                }
                break;
            case R.id.btnResta:
                if (!txt1.getText().toString().isEmpty() && !txt2.getText().toString().isEmpty()) {
                    op.setNum1(Float.parseFloat(txt1.getText().toString().trim()));
                    op.setNum2(Float.parseFloat(txt2.getText().toString().trim()));
                    txtRes.setText(Float.toString(op.resta()));
                }

                break;
            case R.id.btnMult:
                if (!txt1.getText().toString().isEmpty() && !txt2.getText().toString().isEmpty()) {
                    op.setNum1(Float.parseFloat(txt1.getText().toString().trim()));
                    op.setNum2(Float.parseFloat(txt2.getText().toString().trim()));
                    txtRes.setText(Float.toString(op.mult()));
                }

                break;
            case R.id.btnDivi:
                if (!txt1.getText().toString().isEmpty() && !txt2.getText().toString().isEmpty()) {
                    op.setNum1(Float.parseFloat(txt1.getText().toString().trim()));
                    op.setNum2(Float.parseFloat(txt2.getText().toString().trim()));
                    txtRes.setText(Float.toString(op.div()));
                }

                break;
            case R.id.btnLimpiar:
                txt1.setText("");
                txt2.setText("");
                txtRes.setText("");

                break;
            case R.id.btnCerrar:
                finish();
                break;
        }

    }


}
